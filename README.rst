
~~~~~~~~~~~
m4version
~~~~~~~~~~~

I used to use a m4/version.m4 file to manage versioning in autoconf projects.
This is an attempt to use m4/version.m4 file for haskell versioning.
i.e. to view, change and create a m4/version.m4 file.

Its current drawback is that it requires:
``~/share/autom4te/getM4Version.m4``

See the `user stories <https://gitlab.com/agander/m4version/-/blob/master/STORIES.rst>`_ as to why one would use this.
