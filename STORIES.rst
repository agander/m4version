
Whats this for?
===============

Creating, changing or displaying the version number.

Could be done with sed ecc. but using haskell seems like a good exercise.


Flow Paths
----------

a. Default - no params [Case 1.]

Print out the version formatted for ``git_comm`` thus:

``[0.0.6]: WIP: updated STORIES to ReStructuredText``

b. Any of ``--major, --minor`` or ``--rel``

If any of these are supplied then we must/should/could assume that ``m4/version.m4`` exists.

Which assumes that we must create ``m4/version.m4`` and populate it with any combination of ``--major, --minor`` or ``--rel``.

c. Default (no params) version

Probably ``0.0.1``.


Cases
-----

1. Might want solely the version number

``-p/--print`` (Could be the default, in which case, the 'no args' situation.)

Q: Would you want a line feed or not?

2. Might want to change the major part

(eg. '0.1' of '0.1.12'):
``-m/--maj +1.0``: add 1.0 to 0.1 = 1.1 (hmm, that needs some thought)

3. Change the minor part

(.12 of 0.1.12):
``-m/--min +10``: add 10 to .12 = .22

4. Create new m4/version.m4

If doesnt exist:
``-c/--create 0.1.0``

Or, supply the values one wants, e.g. 0.0.12 and let the process just do do its
thing.
i.e. Create the necessary dir+file and write to it.
``--create`` wouldnt be neccessary.

Will require some trial & error.

5. ``--directory``

An option to run setup_m4_version on a particular directory other than the current one.

a] If ``--directory`` exists then check, create ``m4/`` and ``version.m4`` within it.
Respect ``--ma, --mi`` and ``--rel``.

b] If 


6. ``--debug``

Always thinking of this but never achieving/not yet achieved what I would like.
