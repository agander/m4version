{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE TemplateHaskell #-}

module Internal
(   putNPrint
  , check_create_m4
  , isSubseqOf
  , isLongArg
  , m4VersionString
  , Opts(..)
  , Command(..)
  , optsParser
  , versionOption
  , programOptions
  --, VP.Parser(..)
  --, panic
  )
where
--import qualified Data.Text as T
import System.IO (hPutStrLn, stderr)
import System.Directory (listDirectory
                       , doesDirectoryExist
                       , doesFileExist
                       , createDirectory)

import Data.List -- ((!!))
import Options.Applicative
import Data.Semigroup ((<>))

import Data.Text (Text)
import qualified Data.Text as T
import qualified Text.Megaparsec as M
import qualified Text.Megaparsec.Char as MC

import Data.Void
import qualified Text.Megaparsec.Char.Lexer as L

import Text.Megaparsec.Debug
import Text.Printf
--import qualified ValueP as VP

import Data.Version (showVersion)
import Development.GitRev (gitHash)
import Paths_m4version (version)

isLongArg (x:y:_) = x == '-' && y == '-'
isArg (x:y:_) = x == '-' && y /= '-'

--argsToTuple xs@x:y:ys = [(x : argsToTuple xs)]

data ParseResult = Help | PrintVersion | Run Options | ParseError
  deriving (Eq, Show)

data Verbose = Verbose | NoVerbose
  deriving (Eq, Show)

data Force = Force | NoForce
  deriving (Eq, Show)

data Options = Options {
  optionsVerbose :: Verbose
, optionsForce :: Force
, optionsToStdout :: Bool
, optionsTarget :: Maybe FilePath
} deriving (Eq, Show)

printHelp :: IO ()
printHelp = do
  hPutStrLn stderr $ unlines
    [
        "Usage: setup_m4_version [ --silent ] [ --force | -f ] [ PATH ] [ - ]"
      , "       setup_m4_version --version"
      , "       setup_m4_version --help"
    ]


{-
parseOptions :: [String] -> ParseResult
parseOptions xs = case xs of
  ["--version"] -> PrintVersion
  ["--help"] -> Help
  _ -> case targets of
    Just (target, toStdout) -> Run (Options verbose force toStdout target)
    Nothing -> ParseError
    where
      silentFlag = "--silent"
      forceFlags = ["--force", "-f"]

      flags = [silentFlag] ++ forceFlags
      verbose = if silentFlag `elem` xs then NoVerbose else Verbose
      force = if any (`elem` xs) forceFlags then Force else NoForce
      ys = filter (`notElem` flags) xs

      targets = case ys of
        ["-"] -> Just (Nothing, True)
        ["-", "-"] -> Nothing
        [dir] -> Just (Just dir, False)
        [dir, "-"] -> Just (Just dir, True)
        [] -> Just (Nothing, False)
_ -> Nothing
-}

-- | This should return True if (and only if) all the values in
-- the first list appear in the second list, though they need
-- not be contiguous.
{-
Prelude> isSubseqOf "blah" "blahwoot"
True
Prelude> isSubseqOf "blah" "wootblah"
True
Prelude> isSubseqOf "blah" "wboloath"
True
Prelude> isSubseqOf "blah" "wootbla"
False
Prelude> isSubseqOf "blah" "halbwoot"
False
Prelude> isSubseqOf "blah" "blawhoot"
True

-} 
isSubseqOf :: (Eq a)
           => [a]
           -> [a]
           -> Bool
--isSubseqOf = undefined
isSubseqOf [] [] = False
isSubseqOf _ []  = False
isSubseqOf [] _  = False
isSubseqOf (x:xs) (y:ys) = False

-- | Log message function
--putNPrint :: Show a => Char -> String -> a -> IO ()
putNPrint tipe msg var = do
  case tipe of
    "c" -> putStr ">>> COMMENT: "
    "C" -> putStr ">>> COMMENT: "
    "d" -> putStr "*!* DEBUG: "
    "D" -> putStr "*!* DEBUG: "
    "w" -> putStr "!!! WARNING: "
    "W" -> putStr "!!! WARNING: "
    "e" -> putStr "*** ERROR:   "
    "E" -> putStr "*** ERROR:   "
    "t" -> putStr "!!! TEST:    "
    "T" -> putStr "!!! TEST:    "
    _   -> putStr ">>> INFO:    "
  putStr msg
  putStr " "
  print var

-- | create a m4/ directory if not exist
check_create_m4 :: IO Bool
check_create_m4 = do
  haveM4 <- doesDirectoryExist "m4/"
  case haveM4 of
    True -> putStrLn "directory m4/ exists"
    _    -> createDirectory "m4/" >> putStrLn "created new m4/"
  return haveM4

-- | The structure of the m4/version.m4
-- define([VERSION_NUMBER],[0.0.5])dnl
m4VersionString :: String -> String -> String -> String
m4VersionString major mi release = "define([VERSION_NUMBER],[" ++ 
  major ++ "." ++ mi ++ "." ++ release++ "])dnl\n"

-- | Functions/types for optparse-applicative

data Opts = Opts
    { maj :: !String
    , min :: !String
    , rel :: !String
    , optCommand :: !Command
    }

data Command
    = Create
    | Update

optsParser :: ParserInfo Opts
optsParser =
    info
        (helper <*> versionOption <*> programOptions)
        (fullDesc <> progDesc "setup_m4_version" <>
         header
             "setup_m4_version - a program to manage the version held in m4/version.m4")

versionOption :: Parser (a -> a)
--versionOption = infoOption "0.0.1" (long "version" <> help "Show version")
versionOption = infoOption (concat [showVersion version, "-[", $(gitHash), "]"])
                           (long "version" <> help "Show version")

programOptions :: Parser Opts
programOptions =
    Opts <$> strOption   (long "major-version"  <> 
                          short 'm' <>
                          help "Set the major-version" <> 
                          showDefault <> 
                          value "0" <> 
                          metavar "STRING") <*>
             strOption   (long "minor-version" <>
                          short 'n' <>
                          help "Set the minor-version" <> 
                          showDefault <> 
                          value "0" <> 
                          metavar "STRING") <*>
             strOption   (long "release" <>
                          short 'r' <>
                          help "Set the release" <> 
                          showDefault <> 
                          value "1" <> 
                          metavar "STRING") <*>
    hsubparser (createCommand <> updateCommand)

createCommand :: Mod CommandFields Command
createCommand =
    command
        "create"
        (info (pure Create) (progDesc "Create a m4/ (if it doesnt exist) and a version.m4"))

updateCommand :: Mod CommandFields Command
updateCommand =
    command
        "update"
        (info (pure Update) (progDesc "Update m4/version.m4"))

{-
-- | https://hackage.haskell.org/package/gitrev-1.3.1/docs/Development-GitRev.html
panic :: String -> a
panic msg = error panicMsg
  where panicMsg =
          concat [ "[panic ", $(gitBranch), "@", $(gitHash)
                 , " (", $(gitCommitDate), ")"
                 , " (", $(gitCommitCount), " commits in HEAD)"
                 , dirty, "] ", msg ]
        dirty | $(gitDirty) = " (uncommitted files present)"
              | otherwise   = ""
-- | parse the value part of either: -m/-n/-r to Maybe return the (value, +/-)
getOperator :: String -> Maybe Int
getOperator [] = Just 0
getOperator (x:xs) = undefined

data Operator 
  = Add
  | Subtract
  -- | Divide
  -- | Multiply

-- | We add the Parser type synonym that we will use to resolve ambiguity in 
-- the type of the parsers:
type VParser = M.Parsec Void Text

digitsP :: M.Parsec Void T.Text Int
digitsP = read <$> ((T.unpack <$> MC.string "0") <|> M.some MC.digitChar)

data Value = Value
  { versionValue :: Int
  , operator :: String
  } deriving (Eq, Show)
-}

