{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}

module Options
    (
      CommandLine(..)
    , commandLine
    , parseCommandLine
    , versionInfo
    ) where

import Data.Data (Data, Typeable)
import Data.Version (showVersion)
import GHC.Generics (Generic)
import Paths_m4version (version)
import Prelude ()
import Prelude.Compat
import Options.Applicative

data CommandLine
    = Create  { jsonFile :: FilePath, outputFile :: FilePath, templateFile :: FilePath }
    | Version
    deriving (Eq, Read, Show, Typeable, Data, Generic)

createOptions :: Parser CommandLine
createOptions = Create <$> measurements <*> outputFile <*> templateFile
  where
    measurements = strArgument $ mconcat
        [metavar "INPUT-JSON", help "Json file to read Criterion output from."]

    outputFile = strArgument $ mconcat
        [metavar "OUTPUT-FILE", help "File to output formatted report too."]

    templateFile = strOption $ mconcat
        [ long "template", short 't', metavar "FILE", value "default",
          help "Template to use for report." ]

parseCommand :: Parser CommandLine
parseCommand =
  (Version <$ switch (long "version" <> help "Show version info")) <|>
  (subparser $
    command "create" (info createOptions (progDesc "Generate create.")))

commandLine :: ParserInfo CommandLine
commandLine = info (helper <*> parseCommand) $
  header versionInfo <>
  fullDesc

parseCommandLine :: IO CommandLine
parseCommandLine = execParser commandLine

versionInfo :: String
versionInfo = "setup_m4_version " ++ showVersion version

