
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE BlockArguments #-}

module ValueP
  where

import Data.Text (Text)
import qualified Data.Text as T
import qualified Text.Megaparsec as M
import qualified Text.Megaparsec.Char as MC

import Data.Void
import qualified Text.Megaparsec.Char.Lexer as L

--import qualified Data.Set as Set
import Text.Megaparsec.Debug
import Text.Printf

import qualified Text.Megaparsec.Char.Lexer as L

-- | We add the Parser type synonym that we will use to resolve ambiguity in 
-- the type of the parsers:
type Parser = M.Parsec Void Text

digitsP :: M.Parsec Void T.Text Int
digitsP = read <$> ((T.unpack <$> MC.string "0") M.<|> M.some MC.digitChar)

signP :: Parser Char
signP = MC.char '+' M.<|> MC.char '-'

{-
data ValueOp = ValueOp
  { versionValue :: Int
  , operator :: Char
  } deriving (Eq, Show)

data OpValue = OpValue
  { operator :: Char
  , versionValue :: Int
  } deriving (Eq, Show)

--alternativeOpVal :: Parser (Char, Char)
alternativeOpVal = M.try valOp M.<|> opVal
  where
    opVal = (,) <$> (MC.char '+' M.<|> MC.char '-') <*> digitsP
    valOp = (,) <$> digitsP <*> (MC.char '+' M.<|> MC.char '-')
-}


alternatives :: Parser (Char, Char)
alternatives = M.try foo M.<|> bar
  where
    foo = (,) <$> MC.char 'a' <*> MC.char 'b'
    bar = (,) <$> MC.char 'a' <*> MC.char 'c'

-- |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- | Taken from: https://markkarpov.com/tutorial/megaparsec.html#lexing
-- Including: sc, lexeme, symbol, integer, float and signedInteger

sc :: Parser ()
sc = L.space
  MC.space1                         -- (2)
  (L.skipLineComment "//")       -- (3)
  (L.skipBlockComment "/*" "*/") -- (4)

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

symbol :: Text -> Parser Text
symbol = L.symbol sc

integer :: Parser Integer
integer = lexeme L.decimal

signedInteger :: Parser Integer
signedInteger = L.signed sc integer

float :: Parser Double
float = lexeme L.float

-- |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

{-M.parseTest (VP.signedInteger <* M.eof) $ T.pack "+43"
data PersonItem = NameResult Text | PhoneResult Text

parsePerson :: Parser Person
parsePerson = do
  first <- (NameResult <$> parseName) <|> (PhoneResult <$> parsePhone)
  case first of
    NameResult n -> do
      p <- parsePhone
      return $ Person n p
    PhoneResult p -> do
      n <- parseName
      return $ Person n p
-}

