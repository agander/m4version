{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main (main) where

import Data.List

import System.IO (hPutStrLn, stderr, writeFile)
import System.Exit(exitSuccess)
import Options.Applicative (execParser)

import qualified Internal as I
import qualified ValueP as VP

import qualified Text.Megaparsec as M
import Data.Text (Text)
import qualified Data.Text as T

import System.Console.CmdArgs.Verbosity

main :: IO ()
main = do
  whenLoud $ putStrLn "D:23"
  (opts :: I.Opts) <- execParser I.optsParser
  whenLoud $ putStrLn "D:25"
  m4_existed <- I.check_create_m4
  case I.optCommand opts of
    I.Create -> do
      whenLoud $ hPutStrLn stderr "'create' option chosen."
      writeFile "m4/version.m4" $
        I.m4VersionString (I.maj opts) (I.min opts) (I.rel opts)
    I.Update -> do
      whenLoud $ putStrLn "Update opt"
      --putStrLn (I.maj opts ++ "." ++ I.min opts ++ "." ++ I.rel opts)
      writeFile "m4/version.m4" $
        I.m4VersionString (I.maj opts) (I.min opts) (I.rel opts)
  whenLoud $ putStrLn (I.maj opts ++ "." ++ I.min opts ++ "." ++ I.rel opts)

  whenLoud $ M.parseTest VP.alternatives (T.pack "ac")
  whenLoud $ putStr "D:40:signedInteger: "
  whenLoud $ M.parseTest (VP.signedInteger <* M.eof) $ T.pack "+43"
  whenLoud $ M.parseTest ((VP.symbol (T.pack "+") M.<|> VP.symbol (T.pack "-")) <* M.eof) $ T.pack "+"
  whenLoud $ M.parseTest (VP.float <* M.eof) $ T.pack "123.43"

  -- | GitRev
  --I.panic "Oh no!"
  return ()

