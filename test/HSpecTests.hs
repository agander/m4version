module Main where

import Internal
import Test.Hspec

main :: IO ()
main = hspec $ do

{-
m4VersionString :: String -> String -> String -> String
m4VersionString major mi release = "define([VERSION_NUMBER],[" ++ 
  major ++ "." ++ mi ++ "." ++ release++ "])dnl\n"
-}
  describe "define([VERSION_NUMBER],[0.0.5])dnl" $ do
    it "define([VERSION_NUMBER],[0.0.5])dnl" $ do
      m4VersionString "0" "0" "5" `shouldBe` "define([VERSION_NUMBER],[0.0.5])dnl\n"

  describe "define([VERSION_NUMBER],[1234567890.0.5])dnl" $ do
    it "define([VERSION_NUMBER],[1234567890.0.5])dnl" $ do
      m4VersionString "1234567890" "0" "5" `shouldBe` "define([VERSION_NUMBER],[1234567890.0.5])dnl\n"

  describe "define([VERSION_NUMBER],[0.0.-5])dnl" $ do
    it "define([VERSION_NUMBER],[0.0.-5])dnl" $ do
      m4VersionString "0" "0" "-5" `shouldBe` "define([VERSION_NUMBER],[0.0.-5])dnl\n"

  describe "define([VERSION_NUMBER],[0.0.5-f32])dnl" $ do
    it "define([VERSION_NUMBER],[0.0.5-f32])dnl" $ do
      m4VersionString "0" "0" "5-f32" `shouldBe` "define([VERSION_NUMBER],[0.0.5-f32])dnl\n"

  describe "define([VERSION_NUMBER],[z.0.5-f32])dnl" $ do
    it "define([VERSION_NUMBER],[z.0.5-f32])dnl" $ do
      m4VersionString "z" "0" "5-f32" `shouldBe` "define([VERSION_NUMBER],[z.0.5-f32])dnl\n"

